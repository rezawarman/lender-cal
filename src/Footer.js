import React, {Component} from 'react';

export default class Footer extends Component {
    render() {
        return (
            <footer class="footer">
                <div class="container">
                    <div class="text-center">
                        Copyright © 2018 Reza Mahardityawarman
                </div>
                </div>
            </footer>
        );
    }
}